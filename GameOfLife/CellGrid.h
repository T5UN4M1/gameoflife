#pragma once
#include "Matrix.h"
#include "Config.h"

#include <SFML/Graphics.hpp>

class CellGrid
{
protected:
	Matrix<char> matrix;
	sf::VertexArray array;

	Config config;

	std::unique_ptr<sf::RenderWindow> window;

	unsigned frame;

public:
	CellGrid(Config conf);

	void init();
	void run();
	void yield();
	void iteration();

	void multiThreadedIteration();
	static void processRow(unsigned y, std::vector<sf::Vector2i>* changes, const CellGrid* grid);


	void initVertexArray();
	void toggleCell(unsigned x, unsigned y);
	void setCell(unsigned x, unsigned y, bool alive);
	bool isAlive(unsigned x, unsigned y)const;
	bool isAliveSafe(unsigned x, unsigned y)const;

	const Matrix<char>& getMatrix()const;
	const Config& getConfig()const;

	void importInitFile();
};

