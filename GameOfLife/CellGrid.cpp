#include "CellGrid.h"
#include "Util.h"
#include <iostream>
#include <fstream>
#include <thread>


CellGrid::CellGrid(Config config):config(config),matrix(Matrix<char>(config.xSize, config.ySize, 0)),array(sf::VertexArray(sf::Quads,config.xSize*config.ySize*4))
{
}
void CellGrid::init() {
	window = std::make_unique<sf::RenderWindow>(sf::VideoMode::getDesktopMode(), "Game of life", sf::Style::Default);
	if (config.fps == 60) {
		window->setVerticalSyncEnabled(true);
	}
	else {
		window->setFramerateLimit(config.fps);
	}
	window->setMouseCursorVisible(false);
	window->setMouseCursorGrabbed(true);

	//v line 
	/*for (unsigned i = 5; i < 95; ++i) {
		setCell(50, i,true);
	}*/
	/*
	//glider
	toggleCell(2, 1);
	toggleCell(3, 2);
	for (int i = 0; i < 3; ++i) {
		toggleCell(i+1, 3);
	}
	*/
	
	importInitFile();
	initVertexArray();

}
void CellGrid::run()
{
	init();
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::F12)){
				window->close();
			}
		}

		window->clear();

		yield();

		window->display();
	}
}

void CellGrid::yield()
{
	if (frame%config.fpi == 0 && frame > 100) {
		iteration();
	}
	window->draw(array);
	++frame;
}

void CellGrid::iteration()
{
	vector<sf::Vector2i> changes;

	for (unsigned y = 0; y < matrix.ySize(); ++y) {
		for (unsigned x = 0; x < matrix.xSize(); ++x) {
			unsigned aliveCells = 0;
			for (int j = -1; j < 2; ++j) {
				for (int i = -1; i < 2; ++i) {
					if (j == 0 && i == 0) {
						continue;
					}
					if (isAliveSafe(x + i, y + j)) {
						++aliveCells;
					}
				}
			}
			if (isAlive(x, y)){ // cell is alive
				if (aliveCells < config.underPopulation || aliveCells > config.overPopulation) {
					changes.push_back(sf::Vector2i(x, y));
				}
			}
			else if (aliveCells == config.reproduction) { // cel is dead
				changes.push_back(sf::Vector2i(x, y));
			}
		}
	}
	for (auto& c : changes) {
		toggleCell(c.x, c.y);
	}
}

void CellGrid::multiThreadedIteration() {
	// thread implementation (not working)
	/*
	std::vector<std::thread> pool;
	std::vector<std::vector<sf::Vector2i>>* changes = new std::vector<std::vector<sf::Vector2i>>();
	for (unsigned i = 0; i < matrix.ySize(); ++i) {
		changes->push_back(std::vector<sf::Vector2i>());
		pool.push_back(std::thread(&processRow, i,changes[i],this));
	}
	for (unsigned i = 0; i < matrix.ySize(); ++i) {
		pool[i].join();
	}
	for (unsigned i = 0; i < matrix.ySize(); ++i) {
		for (auto& c : (*changes)[i]) {
			toggleCell(c.x, c.y);
		}
	}
	delete changes;
	*/
}
void CellGrid::processRow(unsigned y,std::vector<sf::Vector2i>* changes,const CellGrid* grid) {
	for (unsigned x = 0; x < grid->getMatrix().xSize(); ++x) {
		unsigned aliveCells = 0;
		for (int j = -1; j < 2; ++j) {
			for (int i = -1; i < 2; ++i) {
				if (j == 0 && i == 0) {
					continue;
				}
				if (grid->isAliveSafe(x + i, y + j)) {
					++aliveCells;
				}
			}
		}
		if (grid->isAlive(x, y)) { // cell is alive
			if (aliveCells < grid->getConfig().underPopulation || aliveCells > grid->getConfig().overPopulation) {
				changes->push_back(sf::Vector2i(x, y));
			}
		}
		else if (aliveCells == grid->getConfig().reproduction) { // cel is dead
			changes->push_back(sf::Vector2i(x, y));
		}
	}
}

void CellGrid::initVertexArray()
{
	sf::Vector2f cs = sf::Vector2f(config.dxSize / config.xSize,config.dySize / config.ySize); // cellSize

	for (unsigned y = 0; y < config.ySize; ++y) {
		for (unsigned x = 0; x < config.xSize; ++x) {
			unsigned cell = (x + y * config.xSize) * 4;
			array[cell].position     = sf::Vector2f(cs.x * x,     cs.y * y);
			array[cell + 1].position = sf::Vector2f(cs.x * (x+1), cs.y * y);
			array[cell + 2].position = sf::Vector2f(cs.x * (x+1), cs.y * (y+1));
			array[cell + 3].position = sf::Vector2f(cs.x * x,     cs.y * (y+1));

			sf::Color c = isAlive(x, y) ? sf::Color::White : sf::Color::Black;
			if (isAlive(x, y)) {

			}
			for (unsigned i = 0; i < 4; ++i) {
				array[cell + i].color = c;
			}
		}
	}
}

void CellGrid::toggleCell(unsigned x, unsigned y)
{
	setCell(x, y, !isAlive(x, y));
}

void CellGrid::setCell(unsigned x, unsigned y, bool alive)
{
	matrix.set(x, y, alive ? 1:0);
	unsigned cell = (x + y * config.xSize) * 4;
	sf::Color c = alive ? sf::Color::White : sf::Color::Black;
	for (unsigned i = 0; i < 4; ++i) {
		array[cell + i].color = c;
	}
}

bool CellGrid::isAlive(unsigned x, unsigned y) const
{
	return matrix.get(x,y) == 1;
}

bool CellGrid::isAliveSafe(unsigned x, unsigned y) const
{
	return x > 0 && x < matrix.xSize() && y>0 && y < matrix.ySize() && matrix.get(x, y) == 1;
}

const Matrix<char>& CellGrid::getMatrix() const
{
	return matrix;
}

const Config & CellGrid::getConfig() const
{
	return config;
}

void CellGrid::importInitFile()
{
	std::ifstream file("./init.txt");
	if (file.is_open()) {
		std::string line;
		getline(file, line);
		if (line == "MODE_COORDINATES") {
			while (getline(file, line)) {
				if (contains(line, ">")) {
					auto parts = explode(line, '>');
					if (parts.size() != 2) {
						continue;
					}
					sf::Vector2i p1 = parseCoordinate(parts[0]);
					sf::Vector2i p2 = parseCoordinate(parts[1]);
					if (p1.x > p2.x || p1.y > p2.y) {
						continue;
					}
					for (; p1.x <= p2.x; ++p1.x) {
						for (; p1.y <= p2.y; ++p1.y) {
							toggleCell(p1.x, p1.y);
						}
					}
				}
				sf::Vector2i p = parseCoordinate(line);
				toggleCell(p.x, p.y);
			}
		} else {
			unsigned x = 0;
			unsigned y = 0;
			do {
				for (char& c : line) {
					if (c == '1') {
						toggleCell(x, y);
					}
					++x;
				}
				x = 0;
				++y;
			} while (getline(file, line));
		}

	}
	else {
		std::cout << "Error opening init file" << std::endl;
	}
}


