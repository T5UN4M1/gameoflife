#include <SFML/Graphics.hpp>
#include "Config.h"
#include "CellGrid.h"
int main()
{

	Config config(ConfigSetting::CONFIG_FILE);

	CellGrid grid(config);

	grid.run();
	return 0;
}