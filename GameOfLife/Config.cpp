#include "Config.h"

#include <iostream>
#include <fstream>
#include "Util.h"


using std::cout;
using std::endl;
using std::stoi;

void Config::configToConsole() const
{
	std::string sep = ":";
	cout << xSize << sep << ySize << sep << dxSize << sep << dySize << sep << fps << sep << fpi << sep << underPopulation << sep << overPopulation << sep << reproduction << endl;
}

Config::Config(ConfigSetting s)
{
	switch (s) {

	case DEFAULT:
		xSize = 30;
		ySize = 30;

		dxSize = 1920;
		dySize = 1080;

		fps = 60;
		fpi = 6;

		underPopulation = 2;
		overPopulation = 3;
		reproduction = 3;

		break;
	case CONFIG_FILE:
		std::ifstream file("./conf.txt");
		if (file.is_open()) {
			std::string line;
			unsigned a;
			for (unsigned i = 0; i < 9; ++i) {
				getline(file, line);
				a = stoi(line);
				switch (i) {
				case 0:
					xSize = a;
					break;
				case 1:
					ySize = a;
					break;
				case 2:
					dxSize = a;
					break;
				case 3:
					dySize = a;
					break;
				case 4:
					fps = a;
					break;
				case 5:
					fpi = a;
					break;
				case 6:
					underPopulation = a;
					break;
				case 7:
					overPopulation = a;
					break;
				case 8:
					reproduction = a;
					break;
				}
			}
		}
		else {
			cout << "Fatal error, couldn't open config file conf.txt" << endl;
		}
		break;
	}
}


Config::~Config()
{
}
