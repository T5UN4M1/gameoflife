#pragma once
#include <string>
#include <sstream>

#include <iostream>

#include <SFML/Graphics.hpp>

template<class T>
std::string toStr(T nb) {
	std::stringstream ss;
	ss << nb;
	return ss.str();
}

std::vector<std::string> explode(std::string const & s, char delim);
bool contains(std::string stack, std::string needle);
sf::Vector2i parseCoordinate(std::string coord);