#include "Util.h"

std::vector<std::string> explode(std::string const & s, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(s);

	for (std::string token; std::getline(iss, token, delim); )
	{
		result.push_back(std::move(token));
	}

	return result;
}

bool contains(std::string stack, std::string needle)
{
	return stack.find(needle) != std::string::npos;
}
sf::Vector2i parseCoordinate(std::string coord) {
	auto c = explode(coord, ',');
	if (c.size() != 2) {
		return sf::Vector2i(0,0);
	}
	return sf::Vector2i(std::stoi(c[0]), std::stoi(c[1]));
}