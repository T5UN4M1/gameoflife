#pragma once
#include <utility>

using std::pair;
using std::make_pair;

enum ConfigSetting {
	DEFAULT,
	CONFIG_FILE
};
class Config
{
public:
	unsigned xSize; // board size
	unsigned ySize;

	unsigned dxSize; // display size (window)
	unsigned dySize;

	unsigned fps; // frame per second, 60 enables vSync
	unsigned fpi; // frame per iteration

	unsigned underPopulation; // if a living cell has less than X neighbors , it dies
	unsigned overPopulation; // if a living cell has more than X neighbors , it dies
	unsigned reproduction; // if a dead cell has X neighbors, it becomes alive

	void configToConsole()const;
	Config(ConfigSetting s);
	~Config();
};

